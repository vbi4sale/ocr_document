ocr_document ios using we scan
example


    scan.scanDoc(successCallback, errorCallback, {sourceType : 1, fileName : "myfilename", quality : 1.0, returnBase64 : false}); 
    // sourceType will by default take value 1 if no value is set | 0 for gallery | 1 for camera. 
    // fileName will take default value "image" if no value set. Supported only on 4.x.x plugin version
    // quality will take default value 1.0 (highest). Lowest value is 5.0. Any value in between will be accepted
    // returnBase64 will take default boolean value false, meaning image URL is returned. If true base64 is returned
    function successCallback(imageData) {
        alert(imageData);
        console.log(imageData);
        //var image = document.getElementById('myImage');
        //image.src = imageData; // Image URL rendering. 
	//image.src = imageData + '?' + Date.now(); // For iOS, use this to solve issue 10 if unique fileName is not set.
	//image.src = "data:image/jpeg;base64," + imageData; // Base64 rendering
    }

    function errorCallback(message) {
        alert('Failed because: ' + message);
    }
